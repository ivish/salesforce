import React from 'react';
import{
    Alert,
    AsyncStorage,
    View,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native'
import Camera from 'react-native-camera';
import Detail from './detail.component';
import global from './global';
import Unpaid from './unpaid.component';
import Spinner from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { USER_PARAMS } from './constants';

let baseURL = null;

export default class QRScan extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const params = this.props;
        if(params.instance_url == undefined) {
            Alert.alert("Can't find correct login info. Try login first.");
            Actions.Login();
            return;
        }
        const get_url = params.instance_url + '/services/apexrest/api/getURL';
        const auth_data = params.token_type + ' ' + params.access_token;
        fetch(get_url, {
            method: "GET",
            headers: { "Authorization": auth_data }
        })
        .then((resp) => resp.json()).then(jsonData => {
            jsonData = jsonData.replace("http:", "https:");
            baseURL = jsonData;
            if(baseURL){
                global.scanFlag = false;
            }
            else {
                Alert.alert("Can't find correct login info. Try login first.");
                Actions.Login();
                return;
            }
        })
        .catch(err => {
            Alert.alert('Something went wrong. Please try again')
        })
        .done();
    }
    

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.headerContainer}>
                <Text style={styles.headerText1}>Scan a Ticket's QR Code</Text>
            </View>
            <View style={styles.cameraContainer}>
                <Camera onBarCodeRead={this.onBarCodeRead} style={styles.camera}>
                <View style={styles.rectangleContainer}>
                    <View style={styles.rectangle}/>
                </View>
                </Camera>
            </View>
            </View>
        );
    }
    
    onBarCodeRead = (result) => {
        if (!global.scanFlag) {
            global.scanFlag = true;
            const qr_id = result.data; //might make the url with form
            this.getUserInfo(qr_id);
        }
    }

    getUserInfo = (qrId) => {
        const qr_id = qrId;
        const my_url = baseURL + '/services/apexrest/api/registration/' + qr_id;
        fetch(my_url)
        .then((resp) => resp.json())
        .then((jsonData) => {
            if(jsonData.status.includes('Registered')) {
                setTimeout(() => {
                    global.scanFlag = false;
                }, 3000);


                const uparams = {
                    name: jsonData.name,
                    email: jsonData.email,
                    address: jsonData.address,
                    phone: jsonData.phone,
                    remainingAmount: jsonData.Remaining_Amount,
                    status: 'unpaid',
                    url: baseURL,
                    qrID: qr_id,
                    startTime: jsonData.startDate,
                    eventName: jsonData.event
                };
               
                uparams.remainingAmount = uparams.remainingAmount ? uparams.remainingAmount : 0;
                AsyncStorage.setItem(USER_PARAMS, JSON.stringify(uparams))
                Actions.Unpaid(uparams);   
                return;
            } else {
                Alert.alert(
                    'Already Paid',
                    'Already paid and get ready for next sign',
                    [
                        {text: 'Cancel'},
                        {text: 'OK'},
                    ]
                );
                setTimeout(() => {
                    global.scanFlag = false;
                }, 3000);
            }
        })
        .catch(err => {
            Alert.alert('Something went wrong. Please try again')
        })
        .done();
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#F7F7F9'
    },
    headerContainer: {
      backgroundColor: '#3887E0',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: 0.20 * Dimensions.get('window').height
    },
    headerText1: {
        color: '#F7F6F8',
        fontFamily: 'HelveticaNeue',
        fontSize: 28
    },
    cameraContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0.60
    },
    camera: {
      height: 250,
      width: 250,
    },
  
    rectangleContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent',
    },
  
    rectangle: {
      height: 250,
      width: 250,
      borderWidth: 2,
      borderColor: '#F7F7F8',
      backgroundColor: 'transparent',
    }
  });