import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  Dimensions,
  Switch
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import global from './global';

export default class Success extends Component {
    constructor(props){
        super(props);
    }
    
    scanAnother = async () => {
        const result = await AsyncStorage.getItem('HOME_PARAMS');
        Actions.Home(JSON.parse(result))
    }

    toNameCase(text) {
        if (!text) return 'NA';
        const a = text.split(' ');
        if (a) {
            const b = a.map(val => `${val[0].toUpperCase()}${val.slice(1)}` )
            return b.join(' ');
        } else {
            return 'NA';
        }
    }

    formatDate(startTime) {
        if (startTime) {
            try{
                const d = new Date(startTime);
                return `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`
            } catch(err){
                return startTime;
            }
        }
        return null;
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        const params = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.banner}>
                    <Text style={styles.t1}>Payment Success!</Text>
                    <Text style={styles.t2}>${params.remainingAmount} by {this.toNameCase(params.name)}</Text>
                    <View style={styles.t3Wrapper}>
                        <Text style={styles.t3}>{this.toNameCase(params.eventName)}</Text>
                        <Text style={styles.t3}>{this.formatDate(params.startTime)}</Text>
                        <Text style={styles.t3}>{this.toNameCase(params.address)}</Text>
                    </View>
                </View>
                <TouchableOpacity style={styles.loginButtonContainer} activieOpacity={.5} onPress={this.scanAnother}>
                    <Text style={styles.loginButtonText}>SCAN ANOTHER TICKET</Text>
                </TouchableOpacity>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F7F7F9'
    },
    banner: {
        marginTop: 0.20 * Dimensions.get('window').height,
        height: 0.30 * Dimensions.get('window').height,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    t1: {
        fontFamily: 'HelveticaNeue',
        color: '#3887E0',
        fontSize: 27
    },
    t2: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#6D778D',
        fontSize: 16
    },
    t3Wrapper: {
        alignItems: 'center'
    },
    t3: {
        fontFamily: 'HelveticaNeue',
        color: '#6D778D',
        fontSize: 17,
        marginTop: 5
    },
    loginButtonContainer: {
        backgroundColor: "#F59B30",
        height: 44,
        flexDirection: 'row'
    },
    loginButtonText: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        color: '#FFFFFF',
        fontFamily: 'HelveticaNeue-Bold',
        textAlign: 'center'
    }
});