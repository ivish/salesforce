import React, { Component } from 'react';
import {
    StyleSheet,
    Alert,
    Text,
    View, 
    WebView,
    Image,
    TouchableOpacity,
    Dimensions,
    AsyncStorage
} from 'react-native';
import global from './global';
import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  DefaultAnimation,
} from 'react-native-popup-dialog';
import { Actions } from 'react-native-router-flux';
const success_icon = require("../image/paid.jpg");
var WEBVIEW_REF = 'webview';
export default class Detail extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const params = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerText1}>Payment</Text>
                    <Text style={styles.headerText2}>Amount Due for ${params.remainingAmount}</Text>
                </View>
                <WebView
                    ref={WEBVIEW_REF}
                    source={{uri:  params.url }}
                    style={styles.webview}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                    javaScriptEnabled={true}
                    startInLoadingState={false}
                />
            </View>
        );
    }
    reload() {
        this.refs[WEBVIEW_REF].reload();
    }
    componentDidMount() {
        this.reload();
    }
    componentWillUnmount() {
        global.scanFlag = false;
    }


    async onNavigationStateChange(webViewState) {
        var cur_url = webViewState.url.toString();
        var catch_url = "registration";
        if(cur_url.includes(catch_url))
        {
            fetch(cur_url).then((resp) => resp.json()).then(async (jsonData) => {
                if(jsonData.attended == true)
                {
                    const result = await AsyncStorage.getItem('USER_PARAMS');
                    Actions.Success(JSON.parse(result));
                }
                else{
                    alert("Payment Failed");
                }
            }).done();
            this.refs[WEBVIEW_REF].stopLoading();
            return false;
        }
        return true;
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F7F7F9'
    },
    headerContainer: {
        backgroundColor: '#3887E0',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 0.28 * Dimensions.get('window').height
    },
    headerText1: {
        color: '#F7F7F8',
        fontFamily: 'HelveticaNeue',
        fontSize: 28
    },
    headerText2: {
        opacity: 0.75,
        color: '#F7F7F8',
        fontFamily: 'HelveticaNeue',
        lineHeight: 44,
        fontSize: 15
    },
    success_popup: {
        flex: 1,
        resizeMode: 'contain',
        marginHorizontal: 30,
        width: 300,
        height: 200,
        alignItems: "center",
    },
    webview: {
        flex: 1,
    },
    allview: {
        flex: 1
    },
    success_image: {
        flex: 1,
    },
    next_scan: {
        alignItems: "center",
        backgroundColor: "#d73352",
        marginHorizontal: 110,
        height: 40,
        width: 150,
        justifyContent:"center"
    },
});

