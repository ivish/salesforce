import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Switch,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { HOME_PARAMS } from './constants';


export default class Paid extends Component {
    constructor(props){
        super(props);
    }
  
    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    scanAnother = async () => {
        const result = await AsyncStorage.getItem(HOME_PARAMS);
        Actions.Home(JSON.parse(result))
    }

    recievePayment = () => {
        const params = this.props;
        var url = params.url + '/?id=' + params.qrID;
        Actions.Detail({ url: url, rom:'unpaid', remainingAmount: params.remainingAmount });
        return;
    }

    toNameCase(text) {
        if (!text) return 'NA';
        const a = text.split(' ');
        if (a) {
            const b = a.map(val => `${val[0].toUpperCase()}${val.slice(1)}` )
            return b.join(' ');
        } else {
            return 'NA';
        }
    }

    toReadableDateTime(datestr) {
        const a = new Date(datestr);
        if (a) {
            return `${a.toLocaleDateString()}  ${a.toLocaleTimeString()}`
        }
        return datestr
    }

    render() {
        const params = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerText1}>Success!</Text>
                </View>
                <View style={styles.wrapper}>
                    <View style={styles.detailsContainer}>
                        <View style={styles.detailWrapper}>
                            <Text style={styles.detailHeading}>EVENT</Text>
                            <Text style={styles.detailText1}>{this.toNameCase(params.eventName)}</Text>
                            <Text style={styles.detailText}>{this.toReadableDateTime(params.startTime)}</Text>
                            <Text style={styles.detailText}>{this.toNameCase(params.address)}</Text>
                        </View>
                        <View style={styles.detailWrapper}>
                            <Text style={styles.detailHeading}>ATENDEE</Text>
                            <Text style={styles.detailText1}>{this.toNameCase(params.name)}</Text>
                            <Text style={styles.detailText}>{params.email}</Text>
                            <Text style={styles.detailText}>{params.phone}</Text>
                        </View>
                    </View>
                    {
                        (params.remainingAmount) ?
                        <View style={styles.banner}>
                            <Text style={styles.bannerText1}>{this.toNameCase(params.name)} has been marked as attended</Text>
                            <Text style={styles.bannerText2}>${params.remainingAmount} due for his ticket</Text>
                        </View>
                        :
                        <View style={styles.banner}>
                            <Text style={styles.bannerText1}>{this.toNameCase(params.name)} has been marked as attended</Text>
                        </View>
                    }
                </View>
                <View style={styles.buttonsContainer}>
                    {/* {
                        (params.remainingAmount) ?

                        <TouchableOpacity style={styles.buttonContainer1} activieOpacity={.5} onPress={this.recievePayment}>
                            <Text style={styles.buttonText}>RECIEVE PAYMENT</Text>
                        </TouchableOpacity>
                        :
                        null
                    } */}
                    <TouchableOpacity style={styles.buttonContainer1} activieOpacity={.5} onPress={this.recievePayment}>
                            <Text style={styles.buttonText}>RECIEVE PAYMENT</Text>
                        </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonContainer2} activieOpacity={.5} onPress={this.scanAnother}>
                        <Text style={styles.buttonText}>SCAN ANOTHER TICKET</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F7F7F9'
    },
    headerContainer: {
        backgroundColor: '#3887E0',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 0.20 * Dimensions.get('window').height
    },
    headerText1: {
        color: '#F7F7F8',
        fontFamily: 'HelveticaNeue',
        fontSize: 28
    },
    wrapper: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       height: 0.50 * Dimensions.get('window').height
    },
    detailsContainer: {
        width: .60 * Dimensions.get('window').width,
        alignSelf: 'center',
        flex: 0.75
    },
    detailWrapper: {
        flex: 1,
        flexDirection: 'column'
    },
    detailHeading: {
        color: '#3887E0',
        fontSize: 16,
        fontFamily: 'HelveticaNeue-Bold',
        marginBottom: 15
    },
    detailText: {
        fontSize: 13,
        marginTop: 5,
        color: '#57647D',
        fontFamily: 'HelveticaNeue'
    },
    detailText1: {
        fontSize: 13,
        color: '#57647D',
        fontFamily: 'HelveticaNeue-Bold'
    },
    banner: {
        flex: 0.25,
        width: .85 * Dimensions.get('window').width,
        backgroundColor: '#ffffff',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#F7F7F8',
        borderWidth: 2,
        borderRadius: 2
    },
    bannerText1: {
        fontFamily: 'HelveticaNeue',
        fontSize: 16,
        color: '#57647D',
        opacity: 0.9
    },
    bannerText2: {
        marginTop: 5,
        fontFamily: 'HelveticaNeue-Bold',
        color: '#717B90',
        fontSize: 15
    },
    buttonsContainer: {
        flexDirection: 'row'
    },
    buttonContainer1: {
        backgroundColor: "#F59B30",
        height: 44,
        flexDirection: 'row',
        flex: 0.5
    },
    buttonContainer2: {
        backgroundColor: "#A6B1C6",
        height: 44,
        flexDirection: 'row',
        flex: 0.5
    },
    buttonText: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        color: '#FFFFFF',
        fontFamily: 'HelveticaNeue-Bold',
        textAlign: 'center'
    }
});