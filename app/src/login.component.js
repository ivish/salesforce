import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  Dimensions,
  Switch,
  Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import global from './global';
import {
    SANDBOX_CREDS,
    PROD_CREDS,
    HOME_PARAMS,
    SETTINGS_KEY
} from './constants';

const userIcon = require('../image/user.png');
const keyIcon = require('../image/key.png');


export default class Login extends Component {
    constructor(props){
        super(props);
        this.sandboxCreds = SANDBOX_CREDS;
        this.productionCreds = PROD_CREDS;
        this.state = { 
            isAnimating: false, 
            isProduction: true,
            username: '', 
            password: ''
        };
    }

    componentDidMount() {
        AsyncStorage.getItem(SETTINGS_KEY).then((settingsStr) => {
            try {
                const settings = JSON.parse(settingsStr);
                this.setState({ username: settings.username,password: settings.password, client_id: settings.clientId, client_secret: settings.clientSecret, isProduction: settings.is_production });
            } catch(err) {

            }
        });
    }
   
    login = () => {
        if (this.state.isAnimating) return;
        if (!this.state.username || !this.state.password) return;
        this.setState({
            isAnimating: true
        });
        const params = {
            grant_type: 'password', 
            client_id: '',
            client_secret: '',
            username: this.state.username,
            password: this.state.password
        };
        const { clientId, clientSecret, url } = this.state.isProduction ? this.productionCreds : this.sandboxCreds;
        params.client_id = clientId;
        params.client_secret = clientSecret;
        var formData = new FormData();
        for (var k in params) {
            formData.append(k, params[k]);
        }

        fetch(url, {
            method: "POST",
            body: formData
        })
        .then((response) => response.json())
        .then((responseData) => {
            if(responseData.access_token) {
                global.instance_url = responseData.instance_url;
                global.token_type = responseData.token_type;
                global.access_token = responseData.access_token;
                global.username = this.state.username;
                global.password = this.state.password;
                const settingsObj = { username: this.state.username, password: this.state.password, client_id: params.client_id, client_secret: params.client_secret, is_production: this.state.isProduction };
                AsyncStorage.setItem(SETTINGS_KEY, JSON.stringify(settingsObj));
                const hparams  = {
                    instance_url : responseData.instance_url,
                    token_type: responseData.token_type,
                    access_token: responseData.access_token
                };
                AsyncStorage.setItem(HOME_PARAMS, JSON.stringify(hparams))
                Actions.Home(hparams);
            } else {
                Alert.alert('Invalid Username/Password');
                this.setState({
                    username: '',
                    isAnimating: false,
                    password: ''
                })
            }
        })
        .catch(err => {
            Alert.alert('Something went wrong. Please try again');
            this.setState({
                isAnimating: false
            })
        })
        .done();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerText1}>Welcome!</Text>
                    <Text style={styles.headerText2}>Please login using your Salesforce Credentials.</Text>
                </View>
                <View style={styles.inputContainer}>
                    <View style={styles.inputWrap}>
                        <View style={styles.iconWrap}>
                            <Image
                                source={userIcon}
                                style={styles.icon}
                                resizeMode="contain"
                            />
                        </View>
                        <TextInput 
                            placeholder="Username"
                            style={styles.input}
                            onChangeText={(username) => this.setState({username: username})}
                            value={this.state.username}
                        />
                    </View>
                    <View style={styles.inputWrap}>
                        <View style={styles.iconWrap}>
                            <Image
                                source={keyIcon}
                                style={styles.icon}
                                resizeMode="contain"
                            />
                        </View>
                        <TextInput 
                            placeholder="Password"
                            secureTextEntry
                            style={styles.input}
                            onChangeText={(password) => this.setState({password: password})}
                            value={this.state.password}
                        />
                    </View>
                    <View style={styles.switchContainer}>
                        <Text style={{ marginRight: 30, color: '#57647D' }}>Sandbox</Text>
                        <Switch 
                            onValueChange={() => this.setState({ isProduction: !this.state.isProduction })}
                            onTintColor="#F59B30"
                            value={this.state.isProduction}
                        />
                        <Text style={{ marginLeft: 30, color: '#57647D' }}>Production</Text>
                    </View>
                    <ActivityIndicator 
                        animating={this.state.isAnimating}
                        size={'small'}
                    />
                </View>
                <TouchableOpacity style={styles.loginButtonContainer} activieOpacity={.5} onPress={this.login}>
                    <Text style={styles.loginButtonText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#F7F7F9'
    },
    headerContainer: {
        backgroundColor: '#3887E0',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 0.28 * Dimensions.get('window').height
    },
    headerText1: {
        color: '#F7F7F8',
        fontFamily: 'HelveticaNeue',
        fontSize: 28
    },
    headerText2: {
        opacity: 0.75,
        color: '#F7F7F8',
        fontFamily: 'HelveticaNeue',
        lineHeight: 44,
        fontSize: 15
    },
    inputContainer: {
        height: 0.35 * Dimensions.get('window').height,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    inputWrap:{
        flexDirection: 'row'
    },
    input: {
        width: 0.80 * Dimensions.get('window').width,
        height: 44,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#D0D6E1',
        backgroundColor: '#FFFFFF',
        paddingLeft: 3,
        marginLeft: -0.6,
        color: 'grey'
    },
    iconWrap: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: '#D0D6E1',
        borderRightWidth: 0,
        borderRadius: 3,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    icon: {
        width: 20,
        height: 20,
        margin: 5
    },
    switchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    loginButtonContainer: {
        backgroundColor: "#F59B30",
        height: 44,
        flexDirection: 'row'
    },
    loginButtonText: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        color: '#FFFFFF',
        fontFamily: 'HelveticaNeue-Bold',
        textAlign: 'center'
    }
});