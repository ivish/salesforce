/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry, StyleSheet } from 'react-native';
import {Scene, Router} from 'react-native-router-flux';

import QRScan from './app/src/qrscan.component';
import Unpaid from './app/src/unpaid.component';
import Login from './app/src/login.component';
import Detail from './app/src/detail.component';
import Success from './app/src/success.component';

class App extends React.Component {
  render() {
    return (
      <Router navigationBarStyle={styles.navBar} titleStyle={styles.navTitle} >
        <Scene key="root" >
          <Scene key="Login" component={Login} title="Denver JE Registration" initial={true} sceneStyle={styles.scene}/>
          <Scene key="Home" component={QRScan} type="reset" title="Denver JE Registration" sceneStyle={styles.scene}/>
          <Scene key="Unpaid" component={Unpaid} type="reset"  title="Denver JE Registration" sceneStyle={styles.scene}/>
          <Scene key="Detail" component={Detail} type="reset" title="Denver JE Registration" sceneStyle={styles.scene}/>
          <Scene key="Success" component={Success} type="reset" title="Denver JE Registration" sceneStyle={styles.scene}/>
        </Scene>
      </Router>
    );
  }
}
AppRegistry.registerComponent('QRScan', () => App);
const styles = StyleSheet.create({
  navBar: {
    backgroundColor: '#3887E0',
    borderBottomColor: '#F7F7F8',
    borderBottomWidth: 0.4
  },
  navTitle: {
    color: '#ffffff',
    fontSize: 15
  },
  scene: {
    paddingTop: 60,
  }
})